package com.twuc.env.web;


import com.twuc.env.entity.Staff;
import com.twuc.env.repo.StaffRepo;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
public class UserController {


    StaffRepo staffRepo;

    public UserController(StaffRepo staffRepo) {
        this.staffRepo = staffRepo;
    }


    @GetMapping("/api/staff/{id}")
    ResponseEntity getStaff(@PathVariable Long id) {
        Optional<Staff> byId = staffRepo.findById(id);
        if (byId.isPresent()) {
            return ResponseEntity.status(200).body(byId.get());
        }
        return ResponseEntity.notFound().build();
    }
}
