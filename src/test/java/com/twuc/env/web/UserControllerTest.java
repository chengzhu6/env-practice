package com.twuc.env.web;

import com.twuc.env.entity.Staff;
import com.twuc.env.repo.StaffRepo;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;


@SpringBootTest
@AutoConfigureMockMvc
class UserControllerTest {


    @Autowired
    MockMvc mockMvc;





    @Test
    void should_return_200() throws Exception {
        mockMvc.perform(get("/api/staff/1"))
                .andExpect(MockMvcResultMatchers.status().is(404));
    }

//    @Test
//    void should_return_200_and_get_staff_info() throws Exception {
//
//        mockMvc.perform(get("/api/staff/1"))
//                .andExpect(MockMvcResultMatchers.content().string(""));
//    }
}
